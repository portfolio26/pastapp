//
//  HostingController.swift
//  PastApp WatchKit Extension
//
//  Created by Antonio Cimino on 16/01/2020.
//  Copyright © 2020 Antonio Cimino. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<MenuView> {
    override var body: MenuView {
        return MenuView()
    }
}
