import SwiftUI
import UIKit

struct Ingredienti: Identifiable {
    var id = UUID()
    var nome: String
    var image: String
    var quantita: Int
    var tara: String
}

struct Ricetta: Identifiable {
    var id = UUID()
    var nome: String
    var ingredienti: [Ingredienti]
    var descrizione : String
    var tempo: Int
    var posTimer: CGFloat
}


var ricette = [
    Ricetta(nome: "tomato",
            ingredienti:[Ingredienti(nome: "Penne rigate", image: "Pennette",quantita:100,tara:"g"),
                         Ingredienti(nome: "Garlic", image: "Aglio",quantita:1,tara:"clove"), Ingredienti(nome: "Peeled tomato", image: "Sugo",quantita:200,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Olive oil", image: "Olio di oliva",quantita:0,tara:"as needed"),Ingredienti(nome: "Basil", image: "Basilico",quantita:0,tara:"as needed")], descrizione: "tomato_des",tempo:600, posTimer: 5),
    
    Ricetta(nome: "ajeuoj",
               ingredienti:[Ingredienti(nome: "Bucatini", image: "Spaghetti",quantita:100,tara:"g"),
                            Ingredienti(nome: "Olive oil", image: "Olio di oliva",quantita:0,tara:"as needed"),Ingredienti(nome: "Garlic", image: "Aglio",quantita:1,tara:"clove"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Chili pepper", image: "Peperoncino",quantita:0,tara:"as needed")], descrizione: "ajeuoj_des",
                            tempo:600, posTimer: 5),
    
    Ricetta(nome: "4carbonara",
               ingredienti:[Ingredienti(nome: "Spaghetti", image: "Spaghetti",quantita:100,tara:"g"),
                            Ingredienti(nome: "Pork cheek", image: "Bacon",quantita:40,tara:"g"),Ingredienti(nome: "Egg", image: "Uova",quantita:1,tara:"yolk"),Ingredienti(nome: "Cheese", image: "Formaggio",quantita:15,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Pepper", image: "Pepe",quantita:0,tara:"as needed")], descrizione: "carbonara_des",
                            tempo:600, posTimer: 4),
    
    Ricetta(nome: "7bolognese",
               ingredienti:[Ingredienti(nome: "Tagliatelle", image: "Linguine e Tagliatelle",quantita:100,tara:"g"),
                            Ingredienti(nome: "Ground beef", image: "Carne",quantita:50,tara:"g"),Ingredienti(nome: "Carrot", image: "Carota",quantita:1,tara:""),Ingredienti(nome: "Onion", image: "Cipolla",quantita:1,tara:""),Ingredienti(nome: "Celery", image: "Sedano",quantita:2,tara:"stalk"),Ingredienti(nome: "Dry wine", image: "Vino",quantita:0,tara:"as needed"),Ingredienti(nome: "Garlic", image: "Aglio",quantita:1,tara:"clove"),Ingredienti(nome: "Basil", image: "Basilico",quantita:0,tara:"as needed"),Ingredienti(nome: "Peeled tomato", image: "Sugo",quantita:100,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Olive oil", image: "Olio di oliva",quantita:0,tara:"as needed")], descrizione: "bolognese_des",
                            tempo:600, posTimer: 7),
    
    Ricetta(nome: "speck",
               ingredienti:[Ingredienti(nome: "Penne", image: "Pennette",quantita:100,tara:"g"),
                            Ingredienti(nome: "Speck", image: "Bacon",quantita:50,tara:"g"),Ingredienti(nome: "Gorgonzola", image: "Formaggio",quantita:90,tara:"g"),Ingredienti(nome: "Walnuts", image: "Noci",quantita:30,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Pepper", image: "Pepe",quantita:0,tara:"as needed")], descrizione: "speck_des",
                            tempo:600, posTimer: 4),
    
    Ricetta(nome: "tonno",
               ingredienti:[Ingredienti(nome: "Spaghetti", image: "Spaghetti",quantita:100,tara:"g"),
                            Ingredienti(nome: "Tuna", image: "tonno-1", quantita: 60, tara: "g"),
                            Ingredienti(nome: "Olive oil ", image: "Olio di oliva",quantita:0,tara:"as needed"),Ingredienti(nome: "Onion", image: "Cipolla",quantita:1,tara:""),Ingredienti(nome: "Cherry tomatoes", image: "Pomodorini-1",quantita:100,tara:"g"),Ingredienti(nome: "Tomato sauce", image: "Sugo",quantita:50,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Basil", image: "Basilico",quantita:0,tara:"as needed"),Ingredienti(nome: "Pepper", image: "Pepe",quantita:0,tara:"as needed")], descrizione: "tonno_des",
                            tempo:600, posTimer: 6),
    
    Ricetta(nome: "patate-1",
               ingredienti:[Ingredienti(nome: "Mixed pasta", image: "Pasta mista",quantita:100,tara:"g"),
                            Ingredienti(nome: "Celery", image: "Sedano",quantita:1,tara:"stalk"),Ingredienti(nome: "Carrot", image: "Carota",quantita:1,tara:""),Ingredienti(nome: "Onion", image: "Cipolla",quantita:1,tara:""),Ingredienti(nome: "Potatoes", image: "patata",quantita:190,tara:"g"),Ingredienti(nome: "Lard", image: "Bacon",quantita:30,tara:"g"),Ingredienti(nome: "Cheese crust", image: "Formaggio",quantita:0,tara:"as needed"),Ingredienti(nome: "Pepper", image: "Pepe",quantita:0,tara:"as needed")], descrizione: "patate_des",
                            tempo:600, posTimer: 7),
    
    Ricetta(nome: "piselli-1",
               ingredienti:[Ingredienti(nome: "Ditaloni", image: "Rigatoni",quantita:100,tara:"g"),
                            Ingredienti(nome: "Olive oil", image: "Olio di oliva",quantita:0,tara:"as needed"),Ingredienti(nome: "Onion", image: "Cipolla",quantita:1,tara:""),Ingredienti(nome: "Pees", image: "pisello",quantita:250,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Pepper", image: "Pepe",quantita:0,tara:"as needed")], descrizione: "piselli_des",
                            tempo:600, posTimer: 5),
    
    Ricetta(nome: "pescespada",
               ingredienti:[Ingredienti(nome: "Linguine", image: "Linguine e Tagliatelle",quantita:100,tara:"g"),
                            Ingredienti(nome: "Black olives", image: "Olive",quantita:20,tara:"g"),Ingredienti(nome: "Cherry tomatoes", image: "Pomodorini-1",quantita:150,tara:"g"),Ingredienti(nome: "Salt", image: "Sale",quantita:0,tara:"as needed"),Ingredienti(nome: "Garlic", image: "Aglio",quantita:1,tara:"clove"),Ingredienti(nome: "Swordfish", image: "tonno-1",quantita:100,tara:"g"),Ingredienti(nome: "Olive oil", image: "Olio di oliva",quantita:0,tara:"as needed"),Ingredienti(nome: "Pepper", image: "Pepe",quantita:0,tara:"as needed")], descrizione: "pescespada_des",
                            tempo:600, posTimer: 6),
]



struct ContentView: View {
    var r: String
    @State var persona: Int = 0
    @State var points1: String = "vuoto"
    @State var points2: String = "vuoto"
    @State var points3: String = "vuoto"
    @State var points4: String = "vuoto"
    @State var points5: String = "vuoto"
    
    var body: some View {
        GeometryReader { geometry in
        ScrollView(.vertical){
            VStack(spacing: 15){
                
                Image("user1")
                
                
                Text("Choose the serves").foregroundColor(Color.white)
                
                HStack(spacing: 15){
                    Image(self.points1).onTapGesture {
                        self.persona = 1
                        self.points1 = "pieno"
                        self.points2 = "vuoto"
                        self.points3 = "vuoto"
                        self.points4 = "vuoto"
                        self.points5 = "vuoto"
                    }
                    
                    Image(self.points2).onTapGesture {
                        self.persona = 2
                        self.points1 = "pieno"
                        self.points2 = "pieno"
                        self.points3 = "vuoto"
                        self.points4 = "vuoto"
                        self.points5 = "vuoto"
                    }
                    
                    Image(self.points3).onTapGesture {
                        self.persona = 3
                        self.points1 = "pieno"
                        self.points2 = "pieno"
                        self.points3 = "pieno"
                        self.points4 = "vuoto"
                        self.points5 = "vuoto"
                    }
                    
                    Image(self.points4).onTapGesture {
                        self.persona = 4
                        self.points1 = "pieno"
                        self.points2 = "pieno"
                        self.points3 = "pieno"
                        self.points4 = "pieno"
                        self.points5 = "vuoto"
                    }
                    
                    Image(self.points5).onTapGesture {
                        self.persona = 5
                        self.points1 = "pieno"
                        self.points2 = "pieno"
                        self.points3 = "pieno"
                        self.points4 = "pieno"
                        self.points5 = "pieno"
                    }
                    
                }
                Divider().background(Color.yellow)
                VStack {
                    ForEach (ricette) { ric in
                        if(ric.nome == self.r){
                            
                            Image(ric.descrizione).padding(-15)
                            Divider().background(Color.yellow).padding(.top)
                            ForEach (ric.ingredienti) { ingredient in
                                HStack {
                                    Image(ingredient.image).frame(width: 20, height: 25, alignment: .center)
                                            .padding(15)
                                        
                                    Spacer()
                                    
                                    VStack {
                                        Text(ingredient.nome).fontWeight(.bold).frame(width: 105, height: 10, alignment: .leading).font(.system(size: 12))
                                        
                                        Spacer().frame(height: 10)
                                        
                                        if(ingredient.quantita == 0){
                                            Text("\(ingredient.tara)")
                                        .frame(width: 100, height: 10, alignment: .leading).font(.system(size: 15))

                                        } else {
                                        Text("\((ingredient.quantita) * (self.persona)) \(ingredient.tara)")
                                            .frame(width: 100, height: 10, alignment: .leading).font(.system(size: 15))
                                            
                                        }
                                        
                                    }
                                }
                            }
                            NavigationLink(destination: Step1View(nomeRicetta:self.r, tempo:ric.tempo, posTimer:ric.posTimer)) {
                                Text("Start").foregroundColor(Color.black)
                            }.background(Color.yellow).cornerRadius(10).frame(width: 150, height: 50)
                            }
                        }
                    }
                }
            }
        }
        
    }
}


