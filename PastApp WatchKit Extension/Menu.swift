import SwiftUI

struct Cards : Identifiable {
    var id = UUID()
    var image: String
}

struct MenuView: View {
    
    @State var cards = [ Cards(image: "tomato"),
                         Cards(image: "ajeuoj"),
                         Cards(image: "4carbonara"),
                         Cards(image: "7bolognese"),
                         Cards(image: "speck"),
                         Cards(image: "tonno"),
                         Cards(image: "patate-1"),
                         Cards(image: "piselli-1"),
                         Cards(image: "pescespada"),
//                         Cards(image: "ragu")
    ]
    
     var body: some View {
            GeometryReader{ geometry in
            List {
                ForEach(self.cards) { card in
                    NavigationLink(destination: ContentView(r:card.image)) {
                        Image(card.image)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
    //                        .frame(width: geometry.size.width-15)
                    }//.position(x: (geometry.size.width/2)-10, y: 60)
                    
                }
                .onMove{ self.cards.move(fromOffsets: $0, toOffset: $1)}
                .listRowBackground(Color.clear)
                }
                .aspectRatio(contentMode: .fill)
            .listStyle(CarouselListStyle())
    //        .position(x: (geometry.size.width/2)+10, y:geometry.size.height/2)
    //        .frame(width: geometry.size.width+10)
            }
        }
    }

    struct MenuView_Previews: PreviewProvider {
        static var previews: some View {
            MenuView()
        }
    }
