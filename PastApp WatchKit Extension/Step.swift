import WatchKit
import SwiftUI



struct Step : Identifiable{
    var id = UUID()
    var testo: String
}

struct Ricette : Identifiable{
    var id = UUID()
    var nome : String
    var step : [Step]
}

var ricettario = [
    Ricette(nome: "tomato",
            step:[Step(testo: "Put some oil in the pan and put it on the burner."),Step(testo: "Cut the onion in small cubes and put them slowly in the warm oil."),Step(testo: "When the onion is golden colored, add the tomato and cover the pan."),Step(testo: "Fill a pot with some water, then put it on the burner."),Step(testo: "Stir the sauce in the pan and put some salt."),Step(testo: "When the water is boiling, put pasta inside with some salt."),
                  /*Step(testo: "timer")*/Step(testo: "When pasta is ready, drain it and put it in the pan with the sauce."),Step(testo: "Keep cooking until creamy."),Step(testo: "Serve it with a fresh basil leaf.")]),
    
    Ricette(nome: "ajeuoj",
            step:[Step(testo: "Take one and half pieces of garlic and cut it in small slices."),Step(testo: "Take one chili pepper, cut it in small slices."),Step(testo: "Put olive oil in the pan and put it on the burner."),Step(testo: "Fill a pot with water, put on the burner and wait until it boils."),Step(testo: "Add gently the garlic and the chili pepper in the pan with some salt."),Step(testo: "When the water is boiling, put the pasta inside and add some salt."),
                             /*Step(testo: "timer"),*/Step(testo: "When the pasta is ready, drain it and put it in the pan."),Step(testo: "Stir them and add a spoon of cooking water from the pot."),Step(testo: "Serve it and eat when still warm. Enjoy!")]),
               
    Ricette(nome: "4carbonara",
            step:[Step(testo: "Cut the pork cheek and let it melt in the pan on the burner."),Step(testo: "Fill a pot with some water, then put it on the burner."),Step(testo: "When the pork cheek will appear melted add a little cooking water and cook for 10 minutes."),Step(testo: "In a bowl beat the eggs and add cheese and pepper."),Step(testo: "When the water is boiling, put pasta inside with some salt."),/*Step(testo: "timer"),*/
                             Step(testo: "Remember to taste the pasta."),Step(testo: "When pasta is ready drain it and put in the pan with the mixture."),Step(testo: "Turn off the burner and mix eggs and cheese into the pasta for a few seconds."),Step(testo: "Enjoy!")]),
               
    Ricette(nome: "7bolognese",
            step:[Step(testo: "Finely chop carrot, celery, onion, garlic and chili pepper."),Step(testo: "Add 4-5 tablespoons of oil in a pan."),Step(testo: "Pour the vegetables and then brown the meat over medium heat."),Step(testo: "Salt it and leave to flavor for a few minutes, pour the wine and let it evaporate."),Step(testo: "Pass the tomatoes in the vegetable mill and add them to the other ingredients."),Step(testo: "Add the basil leaves, cover it and cook over low heat for a long time."),
                             Step(testo: "For a good ragù it requires at least an hour and half of cooking."),Step(testo: "Boil the tagliatelle in plenty of lightly salted water."),/*Step(testo: "timer"),*/Step(testo: "Drain them into a large bowl and mix well with the ragù.")]),
               
    Ricette(nome: "speck",
          step:[Step(testo: "Cut the speck and let it melt in the pan on the burner."),Step(testo: "Fill a pot with some water, then put it on the burner."),Step(testo: "When the pork cheek will appear melted add gorgonzola and pepper. Let it melt."),Step(testo: "Shell the walnuts and break them in small pieces."),Step(testo: "When the water is boiling, put pasta inside with some salt."),/*Step(testo: "timer"),*/
                           Step(testo: "Remember to taste the pasta."),Step(testo: "When pasta is ready drain it and put in the pan with the mixture."),Step(testo: "Add walnuts and a little more pepper, after turn of the burner."),Step(testo: "Enjoy!")]),
             
    Ricette(nome: "tonno",
            step:[Step(testo: "Put a bit of oil in the pan and put it on the burner."),Step(testo: "Add gently half of an onion cutted in small cubes and put the flame lower."),Step(testo: "Cut 5-6 small tomatoes in 4, put them gently in the pan and cover it."),Step(testo: "Add some tomato sauce and 2 small tuna cans, then add some salt."),Step(testo: "Stir and let it cook for more or less 10 minutes."),Step(testo: "Fill the pot with some water and put it on the burner."),
                             Step(testo: "When the water is boiling, put the spaghetti inside the pot and add some salt."),/*Step(testo: "timer"),*/Step(testo: "When the pasta is ready, dry it in the sink and put in the pan with the sauce."),Step(testo: "Serve it with a basil leaf and a bit of pepper.")]),
               
    Ricette(nome: "patate-1",
            step:[Step(testo: "Chop celery, carrot and onion finely, then chop the lard."),Step(testo: "Clean the potatoes and cut them into irregular pieces of about 2 cm."),Step(testo: "Put a pan on the burner and add some oil, the lard, onion, carrots and celery."),Step(testo: "After a few minutes add the potatoes and the rosemary."),Step(testo: "Add the cheese crust (first you can wash it and scratch the outside)."),Step(testo: "Add also the tomato sauce, mix it with pepper and salt and let it cook for 30 minutes."),
                     Step(testo: "Put some water in the pot and put in on the burner with some salt."),Step(testo: "When the water is boiling, pour the mixed pasta."),/*Step(testo: "timer"),*/Step(testo: "When the pasta is ready, mix everything in the pan and serve with pepper.")]),
       

    Ricette(nome: "piselli-1",
            step:[Step(testo: "Put some oil in the pan and put it on the burner."),Step(testo: "Cut the onion and the bacon in small cubes and put them slowly in the warm oil."),Step(testo: "When the onion is golden colored, put the pees in the pan."),Step(testo: "Add some water and salt, then cover the pan."),Step(testo: "Fill a pot with some water, then put it on the burner."),Step(testo: "When the water is boiling, put pasta inside with some salt."),
                             /*Step(testo: "timer"),*/Step(testo: "When pasta is ready, drain it and put it in the pan with the pees."),Step(testo: "Keep cooking until creamy."),Step(testo: "Serve it with pepper.")]),
               
    Ricette(nome: "pescespada",
            step:[Step(testo: "Start washing cherry tomatoes and cut them into 4 parts."),Step(testo: "Take the swordfish steak, remove the skin and cut it into cubes."),Step(testo: "Put a pot full of water on the fire, salt it and bring it to the boil."),Step(testo: "Pour the olive oil into a pan, add the garlic and let it brown for 2 minutes."),Step(testo: "Remove the garlic and add the tomatoes, salt and pepper them."),Step(testo: "Add a ladle of hot water and also the pitted olives."),
                             Step(testo: "In the meantime, throw the linguine into the boiling water."),/*Step(testo: "timer"),*/Step(testo: "Add the swordfish cubes to the sauce, mix everything and cook for 5 minutes."),Step(testo: "Drain pasta directly in the sauce and mix it well.")]),
            
]





struct Step1View: View {
    
    var nomeRicetta: String
    @State var points1: String = "pieno-1"
    @State var points2: String = "vuoto-1"
    @State var points3: String = "vuoto-1"
    @State var points4: String = "vuoto-1"
    @State var points5: String = "vuoto-1"
    @State var points6: String = "vuoto-1"
    @State var points7: String = "vuoto-1"
    @State var points8: String = "vuoto-1"
    @State var points9: String = "vuoto-1"
    @State private var offset: CGFloat = 0
    @State private var index = 0
    var tempo: Int
    var posTimer: CGFloat
    let spacing: CGFloat = 10
    
    var body: some View {
       
        
          GeometryReader { geometry in
            ZStack{
              return ScrollView(.horizontal, showsIndicators: true) {
                HStack(spacing: self.spacing) {
                    ForEach(ricettario) { r in
                        if(r.nome == self.nomeRicetta){
                            ForEach(r.step){ t in
                                ZStack{

                                    Text(t.testo).fontWeight(.regular).tracking(1).lineLimit(6).truncationMode(.middle).foregroundColor(Color.yellow).frame(width: geometry.size.width, height: geometry.size.height, alignment: .topLeading).offset(y: 10).lineSpacing(-1)
                                }
                            }
                            HStack(spacing: self.spacing){
                                NavigationLink(destination: Cronometro()) {
                                    Text("Set timer").foregroundColor(Color.black)
                                }.background(Color.yellow).cornerRadius(10)
                            }.position(x: (0 - 10 - (geometry.size.width/1.5)) - (geometry.size.width * (8 - self.posTimer)) , y: 77).frame(width: 100, height: 50)
                        }
                    }
                }
                
                
            }
            .content.offset(x: self.offset)
            .frame(width: geometry.size.width, alignment: .leading)
            .gesture(DragGesture()
                .onChanged({ value in
                    self.offset = value.translation.width - geometry.size.width * CGFloat(self.index)
                })
                .onEnded({ value in
                    if -value.predictedEndTranslation.width > geometry.size.width / 2, self.index < ricettario[0].step.count - 1{
                        self.index += 1
                    }
                    if value.predictedEndTranslation.width > geometry.size.width / 2, self.index > 0 {
                        self.index -= 1
                    }
                    switch self.index {
                        case 0:
                            self.points1 = "pieno-1"
                            self.points2 = "vuoto-1"
                        case 1:
                            self.points1 = "vuoto-1"
                            self.points2 = "pieno-1"
                            self.points3 = "vuoto-1"
                        case 2:
                            self.points2 = "vuoto-1"
                            self.points3 = "pieno-1"
                            self.points4 = "vuoto-1"
                        case 3:
                            self.points3 = "vuoto-1"
                            self.points4 = "pieno-1"
                            self.points5 = "vuoto-1"
                        case 4:
                            self.points4 = "vuoto-1"
                            self.points5 = "pieno-1"
                            self.points6 = "vuoto-1"
                        case 5:
                             self.points5 = "vuoto-1"
                             self.points6 = "pieno-1"
                             self.points7 = "vuoto-1"
                        case 6:
                            self.points6 = "vuoto-1"
                            self.points7 = "pieno-1"
                            self.points8 = "vuoto-1"
                        case 7:
                            self.points7 = "vuoto-1"
                            self.points8 = "pieno-1"
                            self.points9 = "vuoto-1"
                        case 8:
                            self.points8 = "vuoto-1"
                            self.points9 = "pieno-1"
                        default:
                            self.points1 = ""
                    }
                    withAnimation { self.offset = -(geometry.size.width + self.spacing) * CGFloat(self.index) }
                })
            )
            }
            HStack(spacing: 4){
            Image(self.points1)
            Image(self.points2)
            Image(self.points3)
            Image(self.points4)
            Image(self.points5)
            Image(self.points6)
            Image(self.points7)
            Image(self.points8)
            Image(self.points9)
            }.position(x: geometry.size.width/2, y: 165)
        }}
}
