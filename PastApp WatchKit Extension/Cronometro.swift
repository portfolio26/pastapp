import SwiftUI

struct Cronometro: View {
    @State var mill: Int = 120
    @Environment(\.presentationMode) var presentationMode

   
    var timer: Timer {
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) {_ in
            self.mill = self.mill - 1
            if(self.mill <= 10 && self.mill > 0){
                WKInterfaceDevice.current().play(.success)
            }
            if(self.mill == 0){
                self.presentationMode.wrappedValue.dismiss()
            }
            self.countDownString()
        }
    }
    
    var body: some View {

        VStack{
            Text(countDownString())
            .font(.system(size: 25))
            .foregroundColor(Color.yellow)
            
            Spacer()
            .frame(height: 10)

            
            HStack{
                
                Text("-").onTapGesture {
                    if(self.mill > 60){
                        self.mill = self.mill - 60
                    }
                    self.countDownString()
                }
                .font(.system(size: 28))
                .foregroundColor(Color.yellow)
                
                Text("START")
                .font(.system(size: 20))
                .fontWeight(.bold)
                .foregroundColor(Color.white)
                .onTapGesture {
                    _ = self.timer
                }
                
                
                Text("+").onTapGesture {
                    self.mill = self.mill + 60
                    self.countDownString()
                }
                .font(.system(size: 28))
                .foregroundColor(Color.yellow)
            }
        }
        
    }

    func countDownString() -> String {
        return String(format: "%02dm:%02ds", mill/60 , mill%60)
    }
    
    
}
