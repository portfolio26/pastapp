//
//  NotificationView.swift
//  PastApp WatchKit Extension
//
//  Created by Antonio Cimino on 16/01/2020.
//  Copyright © 2020 Antonio Cimino. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
#endif
